import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';

export default class ChangePasswordScreen extends React.Component {
  onGoBack = () => {
    this.props.navigation.goBack();
  };

  render() {
    return (
      <View>
        <Text>This is Change Password Screen</Text>
        <TouchableOpacity onPress={this.onGoBack}>
          <Text>Go back</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
