import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {withGlobalContext} from '../../GlobalContextProvider';

class LoginScreen extends React.Component {
  componentDidUpdate(prevProps) {
    console.log(`isSignIn: `, this.props.global.isSignin);
    if (
      this.props.global.isSignin !== prevProps.global.isSignin &&
      this.props.global.isSignin
    ) {
      this.props.navigation.navigate('DrawerNavigation', {
        screen: 'BottomTab',
        params: {
          screen: 'DashboardStack',
          params: {
            screen: 'DashboardScreen',
            params: {
              userName: '123',
              url: 'https://abc'
            },
          },
        },
      });
    }
  }

  onPressLogin = () => {
    const {setSignin} = this.props.global;
    setSignin();
  };

  onPressResetPassword = () => {
    this.props.navigation.navigate('ChangePasswordScreen');
  };

  render() {
    return (
      <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        <Text style={{marginVertical: 10}}>This is Login Screen</Text>
        <TouchableOpacity style={{padding: 10}} onPress={this.onPressLogin}>
          <Text>Login</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{padding: 10}}
          onPress={this.onPressResetPassword}>
          <Text>Reset password</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export default withGlobalContext(LoginScreen);
