import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
export default class DashboardScreen extends React.Component {
  render() {
    console.log(`Data: `, this.props.route.params);
    return (
      <View>
        <Text>
          This is Dashboard Screen with params:{' '}
          {this.props.route.params == undefined
            ? ''
            : this.props.route.params.url}
        </Text>
      </View>
    );
  }
}
