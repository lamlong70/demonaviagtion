import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {withGlobalContext} from '../../GlobalContextProvider';

class SplashScreen extends React.Component {
  componentDidMount() {
    const {setSplash} = this.props.global;
    setTimeout(() => {
      setSplash();
      this.props.navigation.navigate('Authentication', {screen: 'LoginScreen'});
    }, 1000);
  }

  render() {
    return (
      <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        <Text style={{marginVertical: 10}}>This is Splash Screen</Text>
      </View>
    );
  }
}

export default withGlobalContext(SplashScreen);
