import React from 'react';

import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

//Routes
import DashboardStack from '../dashboard/DashboardStack';

//Components

const Tab = createBottomTabNavigator();

export default DrawerNavigator = () => {
  return (
    <Tab.Navigator >
      <Tab.Screen name="DashboardStack" component={DashboardStack} options={{title: 'Tab 1'}}/>
      <Tab.Screen name="DashboardStack2" component={DashboardStack} options={{title: 'Tab 2'}}/>
      <Tab.Screen name="DashboardStack3" component={DashboardStack} options={{title: 'Tab 3'}}/>
    </Tab.Navigator>
  );
};
