import React from 'react';
import {View, Text, TouchableOpacity, SafeAreaView} from 'react-native';

export default class Header extends React.Component {
  onOpenDrawer = () => {
    this.props.navigation.openDrawer();
  };

  render() {
    return (
      <SafeAreaView>
        <View
          style={{
            height: 45,
            backgroundColor: '#FFFFFF',
            borderBottomWidth: 1,
            borderBottomColor: '#E5E5E5',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Text>Header</Text>
          <TouchableOpacity onPress={this.onOpenDrawer}>
            <Text>Open drawer</Text>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    );
  }
}
