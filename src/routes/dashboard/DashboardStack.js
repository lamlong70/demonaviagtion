import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

//Components
import DashboardScreen from '../../screens/dashboard/DashboardScreen';
import Header from './Header';
const Stack = createStackNavigator();

export default DashboardStack = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        header: (props) => {
          return <Header {...props} />;
        },
      }}>
      <Stack.Screen name="DashboardScreen" component={DashboardScreen} />
    </Stack.Navigator>
  );
};
