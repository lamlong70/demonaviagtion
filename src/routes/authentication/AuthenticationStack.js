import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

//Components
import LoginScreen from '../../screens/login/LoginScreen';
import ChangePasswordScreen from '../../screens/login/ChangePasswordScreen';

const Stack = createStackNavigator();

export default AuthenticationStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="LoginScreen" component={LoginScreen} />
      <Stack.Screen name="ChangePasswordScreen" component={ChangePasswordScreen} />
    </Stack.Navigator>
  );
};
